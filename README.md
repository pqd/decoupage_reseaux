## decoupage.py ##

Ce script permet de s'exercer au découpage de sous réseaux.

### Fonctionnement : ###

- présentation d'un réseau existant
- proposition de découpage en X sous réseaux de manière aléatoire.
- demande du nouveau masque CIDR & notation décimale
- demande l'adresse du Nième hôte du Pième sous réseau définis aléatoirement dans les limites autorisées.

